package com.company;

public class Controlador {

    private Entrada entrada = new Entrada();

    public void executar() {
        String forma = entrada.recuperarForma();

        if(forma.equals("circulo")) {
            double diametro = entrada.recuperarDiametro();
            Circulo circulo = new Circulo(diametro);
            circulo.calcularArea();
            Impressora.imprime(circulo);

        } else if(forma.equals("retangulo")) {
            double base = entrada.recuperarBase();
            double altura = entrada.recuperarAltura();
            Retangulo retangulo = new Retangulo(base, altura);
            retangulo.calcularArea();
            Impressora.imprime(retangulo);

        } else if(forma.equals("triangulo")) {
            double ladoA = entrada.recuperarLadoA();
            double ladoB = entrada.recuperarLadoB();
            double ladoC = entrada.recuperarLadoC();
            Triangulo triangulo = new Triangulo(ladoA, ladoB, ladoC);
            triangulo.calcularArea();
            Impressora.imprime(triangulo);
        }
    }
}
