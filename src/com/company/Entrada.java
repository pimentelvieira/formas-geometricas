package com.company;

import java.util.Scanner;

public class Entrada {

    private Scanner scanner = new Scanner(System.in);

    public String recuperarForma() {
        System.out.println("Digite a forma: ");
        return scanner.nextLine();
    }

    public double recuperarLadoA() {
        System.out.println("Digite o lado A do triangulo: ");
        return scanner.nextDouble();
    }

    public double recuperarLadoB() {
        System.out.println("Digite o lado B do triangulo: ");
        return scanner.nextDouble();
    }

    public double recuperarLadoC() {
        System.out.println("Digite o lado C do triangulo: ");
        return scanner.nextDouble();
    }

    public double recuperarDiametro() {
        System.out.println("Digite o diâmetro do círculo: ");
        return scanner.nextDouble();
    }

    public double recuperarBase() {
        System.out.println("Digite a base do retangulo: ");
        return scanner.nextDouble();
    }

    public double recuperarAltura() {
        System.out.println("Digite a altura do retangulo: ");
        return scanner.nextDouble();
    }
}
