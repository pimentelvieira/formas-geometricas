package com.company;

public class FormaGeometrica {
    protected double area;
    protected String nome;

    public FormaGeometrica() {

    }

    public FormaGeometrica(String nome) {
        this.nome = nome;
    }

    public double getArea() {
        return area;
    }

    public String getNome() {
        return nome;
    }
}
