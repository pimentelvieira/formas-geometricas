package com.company;

public class Circulo extends FormaGeometrica implements CalculadoraArea {

    private double diametro;

    public Circulo(String nome, double diametro) {
        super(nome);
        this.diametro = diametro;
    }

    public Circulo(double diametro) {
        this.diametro = diametro;
    }

    @Override
    public void calcularArea() {
        double raio = calcularRaio();
        this.area = Math.PI * (raio * raio);
    }

    private double calcularRaio() {
        return this.diametro / 2;
    }
}
