package com.company;

public class Triangulo extends FormaGeometrica implements CalculadoraArea {

    private double ladoA;
    private double ladoB;
    private double ladoC;

    public Triangulo(String nome, double ladoA, double ladoB, double ladoC) {
        super(nome);
        this.ladoA = ladoA;
        this.ladoB = ladoB;
        this.ladoC = ladoC;
    }

    public Triangulo(double ladoA, double ladoB, double ladoC) {
        this.ladoA = ladoA;
        this.ladoB = ladoB;
        this.ladoC = ladoC;
    }

    private boolean validarTriangulo() {
        return (ladoA + ladoB) > ladoC
                && (ladoA + ladoC) > ladoB
                && (ladoB + ladoC) > ladoA;
    }

    @Override
    public void calcularArea() {
        if (validarTriangulo()) {
            double s = (ladoA + ladoB + ladoC) / 2;
            this.area = Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
        } else {
            System.out.println("Triangulo inválido!");
            System.exit(0);
        }
    }
}
