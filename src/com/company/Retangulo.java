package com.company;

public class Retangulo extends FormaGeometrica implements CalculadoraArea {

    private double base;
    private double altura;

    public Retangulo(String nome, double base, double altura) {
        super(nome);
        this.base = base;
        this.altura = altura;
    }

    public Retangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    @Override
    public void calcularArea() {
        this.area = this.base * this.altura;
    }
}
